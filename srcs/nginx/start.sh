#Create nginx log file
mkdir run/nginx
touch run/nginx/nginx.pid

#Launch OpenSSH service
/usr/sbin/sshd

#Adding user for SSH logging
adduser -D test
echo "test:123" | chpasswd

#authorized_keys adding
mkdir /home/test/.ssh
cp /tmp/id_rbeachserver.pub /home/test/.ssh/authorized_keys

#Launch nginx in silent mode
nginx -g "daemon off;"
